<?php 
    $rand_products = $db->query('SELECT * FROM products WHERE rating>=4 ORDER BY RAND() LIMIT 2')->fetchAll();
?>
            <div class="col-md-3">

                <p class="lead">Categories</p>
                <div class="list-group">
                    <a href="#" class="list-group-item">Category 1</a>
                    <a href="#" class="list-group-item">Category 2</a>
                    <a href="#" class="list-group-item">Category 3</a>
                    <a href="#" class="list-group-item">Category 4</a>
                    <a href="#" class="list-group-item">Category 5</a>
                </div>

                <p class="lead">Featured products</p>
                <?php foreach ($rand_products as $key => $rand_product) { ?>
 
                <div class="product">
                    <div class="thumbnail">
                        <img src="<?=$rand_product['picture'] ?>" alt="">
                        <div class="caption">
                            <h4 class="pull-right"><?= $rand_product['price']?>€</h4>
                            <h4><a href="product.php?id=<?= $rand_product['id']?>"><?= $rand_product['name']?></a>
                            </h4>
                            <p><?= cutString($rand_product['description'], 25) ?></p>
                            <a href="product.php?id=<?= $rand_product['id']?>">Lire la suite</a>
                        </div>
                        <div class="ratings">
                            <p class="pull-right">12 reviews</p>
                            <p>
                            <?php 
                                for($i=1; $i <= $rand_product['rating']; $i++){ ?>
                                <span class="glyphicon glyphicon-star"></span>
                            <?php } ?>
                            </p>
                        </div>
                        <div class="btns clearfix">
                            <a class="btn btn-info pull-left" href=""><span class="glyphicon glyphicon-eye-open"></span> View</a>
                            <a class="btn btn-primary pull-right" href=""><span class="glyphicon glyphicon-shopping-cart"></span> Add to cart</a>
                        </div>
                    </div><!-- /.thumbnail -->
                </div><!-- /.product -->
                <?php } ?>
                

            </div><!-- /.col-md-3 -->